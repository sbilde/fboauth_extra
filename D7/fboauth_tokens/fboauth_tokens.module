<?php

/**
 * Implements hook_menu().
 */
function fboauth_tokens_menu() {
  $items = array();

  $items['user/%user/fboauth'] = array(
    'title' => 'Facebook Authorization',
    'page callback' => 'fboauth_tokens_auth_page',
    'access arguments' => array('authorize fbauth_tokens'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function fboauth_tokens_permission() {
  return array(
    'authorize fbauth_tokens' => array(
      'title' => t('Authorize Facebook application and store access token'),
    ),
  );
}

/**
 * Prints a button to call the authorization action.
 */
function fboauth_tokens_auth_page() {
  $output ='';

  $output .= '<p>'. t('Clicking on the button below will redirect you to Facebook so you can grant this site application the required permissions. If you have already do so recently you will be redirected to this page immediatly, otherwise a page in Facebook will ask you for confirmation and you will be redirected here after that.') .'</p>';
  $output .= '<p>'. t('The authorization will be stored in this website as long as possible, but if at some point the application functionality stops working you should go back to this page and get a new authorization.') .'</p>';

  $output .= fboauth_action_display('fboauth_tokens_authorize_all');

  return $output;
}

/**
 * Implements hook_fboauth_actions().
 */
function fboauth_tokens_fboauth_actions() {
  $actions['fboauth_tokens_authorize_all'] = array(
    'title' => t('Authorize facebook application'),
    'callback' => 'fboauth_tokens_action_authorize',
    // We fill permissions in the _alter hook
    'permissions' => array(),
  );

  return $actions;
}

/**
 * Implements hook_fboauth_actions_alter().
 */
function fboauth_tokens_fboauth_actions_alter(&$actions) {
  /**
   * Since we want to use the token for any action, we have to compile
   * a list of all permissions for all actions declared by modules and
   * get an authorization for all these permissions.
   */
  $permissions = array();
  foreach ($actions as $action) {
    $permissions = array_merge($permissions, $action['permissions']);
  }
  $permissions = array_unique($permissions);

  $actions['fboauth_tokens_authorize_all']['permissions'] = $permissions;
}

/**
 * Gets the access_token from this authorization and stores it in a table
 * that relates users to tokens.
 *
 * This is executed just after the user has granted authorization at FB and
 * is redirected back to the website.
 *
 * //TODO: When fboauth module supports multiple apps we have to also save
 *         the app id and a user could have multiple tokens
 */
function fboauth_tokens_action_authorize($app_id, $access_token) {
  global $user;

  if (!empty($access_token)) {
    $existing_token = fboauth_tokens_get_user_access_token();

    $record = array(
      'uid' => $user->uid,
      'access_token' => $access_token,
    );

    if ($existing_token) {
      $result = drupal_write_record('fboauth_tokens', $record, 'uid');
    }
    else {
      $result = drupal_write_record('fboauth_tokens', $record);
    }

    if ($result == SAVED_NEW || $result == SAVED_UPDATED) {
      drupal_set_message(t('Facebook authorization token for this app saved correctly.'));
    }
    else {
      drupal_set_message(t('There was a problem trying to save your access token.'), 'error');
    }
  }
  else {
    drupal_set_message(t('No access token received from Facebook.'), 'error');
  }

  return 'user/'. $user->uid .'/fboauth';
}

/**
 * Returns the access_token saved for a user uid.
 *
 * If no uid is specified returns the token for the current user.
 */
function fboauth_tokens_get_user_access_token($uid = NULL) {
  global $user;

  static $tokens;

  if (!$uid) {
    $uid = $user->uid;
  }

  if (!isset($tokens[$uid])) {
    $token = db_select('fboauth_tokens', 'fbt')
      ->fields('fbt', array('access_token'))
      ->condition('fbt.uid', $uid)
      ->execute()
      ->fetchField();

    $tokens[$uid] = $token;
  }

  return $tokens[$uid];
}
