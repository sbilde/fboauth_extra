fboauth_extra
=============

Extra modules for FbOAuth module (http://drupal.org/project/fboauth), to save access-tokens and post nodes to facebook walls.

These extra modules are the work of Pedro Lozano (http://drupal.org/user/123766). 



Both modules are at writing time sandbox projects:

Facebook Publish : http://drupal.org/sandbox/pl2/1600946

Facebook OAuth Tokens Store : http://drupal.org/sandbox/pl2/1600932



I have backported them back to drupal6 aswell.
