(function ($) {

Drupal.behaviors.fboauth_publish = function (context) {
  $('#edit-fboauth-publish-post-as').each(function () {
    
    $(this).change(function() {
      $(this).find('option:selected').each(function() {
        if ($(this).attr('value') == 'me') {
          
          $('#edit-fboauth-publish-post-to').find('option').each(function () {
            $(this).removeAttr('disabled');
          });
          
        }
        else {
          var val = $(this).attr('value');
          
          $('#edit-fboauth-publish-post-to').find('option').each(function () {
            if ($(this).attr('value') != val) {
              $(this).attr('disabled', 'disabled');
            }
            else {
              $(this).removeAttr('disabled').attr('selected', 'selected');
            }
          });
        }
      });
    });
    
  });
};

})(jQuery);
